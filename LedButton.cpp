#include <LedButton.h>

LedButton::LedButton(IButtonListener* listener, String identifier, int inputPin, int ledPin)
: Button(listener, identifier, inputPin){
  _timer = StensTimer::getInstance();

  _ledPin = ledPin;
  pinMode(_ledPin, OUTPUT);
}

void LedButton::blink(long interval){
  /* create timer that calls ledOff every 'interval' milliseconds */
  _blinkTimer = _timer->setInterval(this, ACTION_LEDTOGGLE, interval);
}

void LedButton::blinkOnce(long duration){
  ledOn();

  /* create timer that calls ledOff after 'duration' milliseconds */
  _blinkTimer = _timer->setTimer(this, ACTION_LEDOFF, duration);
}

void LedButton::stopBlink(){
  _timer->deleteTimer(_blinkTimer);
  ledOff();
}

void LedButton::ledOn(){
  digitalWrite(_ledPin, HIGH);
  _ledIsOn = true;
}

void LedButton::ledOff(){
  digitalWrite(_ledPin, LOW);
  _ledIsOn = false;
}

void LedButton::toggleLed(){
  if(_ledIsOn){
    ledOff();
  }else{
    ledOn();
  }
}

void LedButton::run(){
  Button::run();
}

void LedButton::timerCallback(Timer* timer){
    if(ACTION_LEDOFF == timer->getAction()){
      ledOff();
    }
    else if(ACTION_LEDTOGGLE == timer->getAction()){
      toggleLed();
    }
}
