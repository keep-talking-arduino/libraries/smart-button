#include <Button.h>

Button::Button(IButtonListener* listener, String identifier, int inputPin){
  _listener = listener;
  _identifier = identifier;
  _inputPin = inputPin;
  pinMode(_inputPin, INPUT);
}

String Button::getIdentifier(){
  return _identifier;
}

bool Button::isPressed(){
  return _pressed;
}

bool Button::equals(Button *button){
  if (_identifier.equals(button->getIdentifier())) {
    return true;
  }
  return false;
}

void Button::activate(){
  _running = true;
}

void Button::deactivate(){
  _running = false;
}

void Button::run(){
  if(_running){ // if button is activated
    if(_pressed){ // if button is already held down
      if(LOW == digitalRead(_inputPin) && millis() - _pressedSince > FAULT_MARGIN){
        _pressed = false;
        _listener->onRelease(this); // call implemented onRelease function
      }
    }else{ // if button is not (yet) held down
      if(HIGH == digitalRead(_inputPin)){
        _pressed = true;
        _pressedSince = millis();
        _listener->onPress(this); // call implemented onPress function
      }
    }
  }
}
