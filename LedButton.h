#ifndef LedButton_H
#define LedButton_H

#include <Arduino.h>
#include <Button.h>
#include <StensTimer.h>

#define ACTION_LEDOFF 1
#define ACTION_LEDTOGGLE 2


class LedButton : public Button, public IStensTimerListener {

public:
  LedButton(IButtonListener* listener, String identifier, int inputPin, int ledPin);
  void blink(long interval);
  void blinkOnce(long duration);
  void stopBlink();
  void ledOn();
  void ledOff();
  void toggleLed();
  void run();

  void timerCallback(Timer* timer);

private:
  StensTimer* _timer;

  int _ledPin;
  bool _ledIsOn = false;
  Timer* _blinkTimer = NULL;
};

#endif
