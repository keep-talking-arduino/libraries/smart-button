#ifndef IButtonListener_H
#define IButtonListener_H

class Button;
class IButtonListener {
  public:
    virtual void onPress(Button* btn) = 0;
    virtual void onRelease(Button* btn) = 0;
};

#endif
