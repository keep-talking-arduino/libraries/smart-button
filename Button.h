#ifndef BUTTON_H
#define BUTTON_H

#include <Arduino.h>
#include <IButtonListener.h>

/* minimal milliseconds that button has to be held down
 to count as a press */
#define FAULT_MARGIN 40

class Button {

public:
  Button(IButtonListener* listener, String identifier, int inputPin);
  bool isPressed();
  bool equals(Button* button);
  String getIdentifier();
  void activate();
  void deactivate();
  void run();

private:
  IButtonListener* _listener;

  String _identifier;
  int _inputPin;
  long _pressedSince;
  bool _pressed = false;
  bool _running = false;

};

#endif
